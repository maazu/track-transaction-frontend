import {useSelector} from 'react-redux';
export const standardHeaderOptions = () => {
  return {
    headers: {
      'Content-Type': 'application/json',
    },
  };
};

export const AuthorisedHeaderOptions = () => {
  const localToken = localStorage.getItem('token');
  return {
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${localToken}`,
    },
  };
};
