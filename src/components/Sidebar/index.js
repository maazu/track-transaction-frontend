import React from 'react';
import {Col} from 'react-bootstrap';
import {Button} from 'react-bootstrap';
import styles from './sidebar.module.scss';
import {Link} from 'react-router-dom';

const Sidebar = () => {
  return (
    <div className={styles['sidebar']}>
      <br />
      <Col md={12} lg={12} className="pt-4 pb-5 d-flex justify-content-center">
        <Link to="new-transaction" style={{textDecoration: 'none'}}>
          <Button className={styles['sidebar-button']} variant="light">
            New Transaction
          </Button>
        </Link>
      </Col>

      <Col md={12} lg={12} className="pb-4 d-flex justify-content-center">
        <Link to="/dashboard" style={{textDecoration: 'none'}}>
          <h6>Dashbaord</h6>
        </Link>
      </Col>
      <Col md={12} lg={12} className="pb-4 d-flex justify-content-center">
        <Link to="transaction" style={{textDecoration: 'none'}}>
          <h6>Transaction</h6>
        </Link>
      </Col>
      <Col md={12} lg={12} className="pb-4 d-flex justify-content-center">
        <Link to="/dashboard/Client" style={{textDecoration: 'none'}}>
          <h6>Client</h6>
        </Link>
      </Col>
      <Col md={12} lg={12} className="pb-4 d-flex justify-content-center">
        <Link to="/dashboard/product" style={{textDecoration: 'none'}}>
          <h6>Product</h6>
        </Link>
      </Col>
      <Col md={12} lg={12} className="pb-4 d-flex justify-content-center">
        <Link to="/reports" style={{textDecoration: 'none'}}>
          <h6>Reports</h6>
        </Link>
      </Col>
      <Col md={12} lg={12} className="pb-4 d-flex justify-content-center">
        <Link to="/settings" style={{textDecoration: 'none'}}>
          <h6>Settings</h6>
        </Link>
      </Col>
    </div>
  );
};

export default Sidebar;
