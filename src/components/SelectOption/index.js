import React from 'react';

const SelectOption = (props) => {
  const options = Object.keys(props.options).map((optionCode, index) => (
    <option key={index} value={optionCode}>
      {props.options[optionCode]}
    </option>
  ));

  return (
    <select className="form-select" name={props.name}>
      <option hidden></option>
      {options}
    </select>
  );
};

export const RequriedSelectOption = (props) => {
  return (
    <select className="form-select" name={props.name} required>
      <option hidden></option>
      {Object.keys(props.options).map((optionCode, index) => (
        <option key={index} value={optionCode}>
          {props.options[optionCode]}
        </option>
      ))}
    </select>
  );
};

export default SelectOption;
