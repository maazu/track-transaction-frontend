import React, {useRef, useState} from 'react';
import {Form, Button} from 'react-bootstrap';
import {useDispatch} from 'react-redux';
import HandleLoginRequest from '../../../api/auth';
import UiNotification from '../../UiNotification';
import LoginAuthUser from '../../../dispatchers/LoginAuthUser';

const SigninForm = () => {
  const dispatch = useDispatch();

  const userNameRef = useRef();
  const userPassRef = useRef();
  const [message, setMesssage] = useState('');
  const [variant, setVariant] = useState('');

  const submitHandler = (event) => {
    event.preventDefault();
    const username = userNameRef.current.value;
    const password = userPassRef.current.value;
    setVariant('sucess');
    setMesssage('Logging in.......');

    HandleLoginRequest({username, password})
      .then(function (result) {
        const response = result['data'];
        console.log(response);
        const token = response['token'];
        const status = response['status'];

        if (token) {
          if (status) {
            LoginAuthUser(dispatch, username, token);
          }
        } else {
          setVariant('error');
          setMesssage(response['message']);
        }
      })
      .catch(function (error) {
        setVariant('error');
        setMesssage('An error occured ' + error);
      });
  };

  return (
    <>
      <Form onSubmit={submitHandler}>
        <UiNotification variant={variant} message={message} />

        <Form.Group className="mb-3">
          <Form.Label>Username</Form.Label>
          <Form.Control type="text" placeholder="email or phone number" ref={userNameRef} required={true} />
        </Form.Group>
        <Form.Group className="mb-3">
          <Form.Label>Password</Form.Label>
          <Form.Control type="password" ref={userPassRef} required={true} />
        </Form.Group>
        <Button variant="primary" type="submit">
          Submit
        </Button>
      </Form>
    </>
  );
};

export default SigninForm;
