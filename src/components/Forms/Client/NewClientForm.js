import React from 'react';
import SelectOption, {RequriedSelectOption} from '../../SelectOption';
import {Row, Col, Form, Button} from 'react-bootstrap';

const NewClientForm = () => {
  const formHandler = (event) => {
    const formData = new FormData(event.currentTarget);
    event.preventDefault();
    const newTransactionDate = JSON.stringify(Object.fromEntries(formData.entries()));
    console.log(newTransactionDate);
    document.getElementById('trans-form').reset();
  };

  return (
    <Form onSubmit={formHandler} id="trans-form">
      <Row>
        <Col md={6} lg={6}>
          <Form.Group className="mb-3">
            <Form.Label>Client Name </Form.Label>
            <Form.Text className="text-muted"> (Reference could be client name or email or phone) </Form.Text>
            <Form.Control type="text" placeholder="Client Name/ Phone/ email" name="client" />
          </Form.Group>
        </Col>
      </Row>
      <Button variant="primary" type="submit" defaultValue="Reset">
        Submit
      </Button>
    </Form>
  );
};

export default NewClientForm;
