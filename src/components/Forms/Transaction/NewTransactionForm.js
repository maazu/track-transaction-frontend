import React from 'react';
import {useState} from 'react';
import SelectOption, {RequriedSelectOption} from '../../SelectOption';
import {Row, Col, Form, Button} from 'react-bootstrap';
import {CreateTransaction} from '../../../api/transaction';
import UiNotification from '../../UiNotification';

import {
  TransactionType,
  TransactionPaymentMethods,
  TransactionStatus,
} from '../../../constants/options/TransactionOptions';

const NewTransactionForm = () => {
  const [message, setMesssage] = useState('');
  const [variant, setVariant] = useState('');

  const formHandler = (event) => {
    event.preventDefault();
    const formData = new FormData(event.currentTarget);

    const newTransactionDate = JSON.stringify(Object.fromEntries(formData.entries()));
    document.getElementById('trans-form').reset();
    CreateTransaction(newTransactionDate)
      .then(function (result) {
        setVariant('sucess');
        setMesssage('transaction has been added ');
      })
      .catch(function (error) {
        setVariant('error');
        setMesssage('An error occured ' + error);
      });
  };

  return (
    <Form onSubmit={formHandler} id="trans-form">
      {<UiNotification variant={variant} message={message} />}
      <Row>
        <Col md={6} lg={6}>
          <Form.Group className="mb-3">
            <Form.Label>Date</Form.Label>
            <Form.Control type="date" name="date" required />
          </Form.Group>
        </Col>
        <Col md={6} lg={6}>
          <Form.Group className="mb-3">
            <Form.Label>Amount</Form.Label>
            <Form.Control type="number" name="amount" placeholder="0.0" step="any" required />
          </Form.Group>
        </Col>
        <Col md={6} lg={6}>
          <Form.Group className="mb-3">
            <Form.Label>Type</Form.Label>
            <RequriedSelectOption name="transactionType" options={TransactionType} />
          </Form.Group>
        </Col>
        <Col md={6} lg={6}>
          <Form.Group className="mb-3">
            <Form.Label>Payment Method</Form.Label>
            <SelectOption options={TransactionPaymentMethods} name="transactionMethod" />
          </Form.Group>
        </Col>
        <Col md={6} lg={6}>
          <Form.Group className="mb-3">
            <Form.Label>Transaction Status</Form.Label>
            <SelectOption options={TransactionStatus} name="transactionStatus" />
          </Form.Group>
        </Col>

        <Col md={6} lg={6}>
          <Form.Group className="mb-3">
            <Form.Label>To/From </Form.Label>
            <Form.Text className="text-muted"> (Reference could be client name or email or phone) </Form.Text>
            <Form.Control type="text" placeholder="(Optional) Client Reference" name="client" defaultValue="" />
          </Form.Group>
        </Col>
      </Row>

      <Button variant="primary" type="submit" defaultValue="Reset">
        Submit
      </Button>
    </Form>
  );
};

export default NewTransactionForm;
