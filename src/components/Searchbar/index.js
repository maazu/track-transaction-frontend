import React from 'react';
import {Form} from 'react-bootstrap';
import styles from './searchbar.module.scss';

const SearchBar = () => {
  return (
    <Form>
      <Form.Group className={'mb-3'}>
        <Form.Control className={styles['searchbar']} type="text" placeholder="Search" />
      </Form.Group>
    </Form>
  );
};

export default SearchBar;
