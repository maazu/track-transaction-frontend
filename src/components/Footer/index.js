import React from 'react';

const Footer = () => {
  return (
    <div className="footer fixed-bottom">
      <p className="text-center"> &copy; All Rights reserved. Ismihan Technologies</p>
    </div>
  );
};

export default Footer;
