import React from 'react';
import styles from './AllTransactionTable.module.css';

export const AllTransactionTable = (props) => {
  return (
    <>
      <table id="customers" className={styles.transaction}>
        <thead>
          <tr>
            <th> Date</th>
            <th> To/From</th>
            <th> Amount</th>
            <th> Type</th>
            <th> Links</th>
            <th> Actions</th>
          </tr>
        </thead>
        <tbody>
          {props.transaction.map((transaction, index) => (
            <tr className={transaction.ID} key={index}>
              <td> {transaction.ID}</td>
              <td> {transaction.Customer}</td>
              <td> {transaction.Amount}</td>
              <td> {transaction.Type}</td>
              <td> Yes</td>
              <td> Modify | Additional Info</td>
            </tr>
          ))}
        </tbody>
      </table>
    </>
  );
};

export default AllTransactionTable;
