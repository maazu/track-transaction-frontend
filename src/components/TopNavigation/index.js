import React from 'react';
import {Container, Row, Col} from 'react-bootstrap';

import {useDispatch} from 'react-redux';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faBell, faQuestionCircle, faCog, faSignOutAlt} from '@fortawesome/free-solid-svg-icons';

import {authActions} from '../../store/auth';
import styles from './topNavigation.module.scss';
import SearchBar from '../Searchbar';

const TopNavigation = () => {
  const dispatch = useDispatch();

  const signout = (event) => {
    event.preventDefault();
    dispatch(authActions.logout());
    window.location.href = '/';
  };

  return (
    <Container fluid>
      <Row className={'mx-auto ' + styles['top-navigation']}>
        <Col md={4} lg={4}>
          <h5 className="fw-bold">Logo + Track Transactions</h5>
        </Col>
        <Col md={4} lg={4}>
          <SearchBar />
        </Col>
        <Col md={4} lg={4} className="clearfix">
          <Row>
            <Col md={6} lg={6} className="float-end"></Col>
            <Col md={6} lg={6} className="float-end">
              <FontAwesomeIcon icon={faBell} size="lg" className="mx-auto" />
              &nbsp; &nbsp;
              <FontAwesomeIcon icon={faQuestionCircle} size="lg" className="mx-auto" /> &nbsp; &nbsp;
              <FontAwesomeIcon icon={faCog} size="lg" className="mx-auto" /> &nbsp; &nbsp;
              <FontAwesomeIcon icon={faSignOutAlt} size="lg" onClick={signout} className="mx-auto" />
            </Col>
          </Row>
        </Col>
      </Row>
    </Container>
  );
};

export default TopNavigation;
