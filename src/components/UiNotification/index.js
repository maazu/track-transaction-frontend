import React from 'react';
import Alert from 'react-bootstrap/Alert';

const UiNotification = (props) => {
  let variant = props.variant;
  const message = props.message;

  if (variant === 'error') {
    variant = 'danger';
  } else {
    variant = 'success';
  }

  return (
    <>
      {message.length > 1 ? (
        <Alert variant={variant}>
          <p>{message}</p>
        </Alert>
      ) : null}
    </>
  );
};

export default UiNotification;
