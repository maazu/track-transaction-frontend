import React from 'react';

import AllTransactionTable from '../../../components/Tables/Trasnactions/AllTransactionTable';

const Transaction = () => {
  const data = [
    {
      ID: 'Quail Mason',
      Amount: '6857',
      Customer: 'adipiscing.elit@google.edu',
      Type: 'Debit',
    },
  ];
  return (
    <>
      <h4>Your Transactions</h4>
      <hr />
      <h4>All Transaction</h4>
      <AllTransactionTable transaction={data} />
    </>
  );
};

export default Transaction;
