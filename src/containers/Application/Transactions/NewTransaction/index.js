import React from 'react';
import {Container} from 'react-bootstrap';
import NewTransactionForm from '../../../../components/Forms/Transaction/NewTransactionForm';

const NewTransaction = () => {
  return (
    <Container>
      <hr />
      <h3>New Transaction Form</h3>
      <hr />
      <NewTransactionForm />
      <hr />
    </Container>
  );
};

export default NewTransaction;
