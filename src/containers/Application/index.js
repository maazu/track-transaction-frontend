import React from 'react';
import {Container, Row, Col} from 'react-bootstrap';

import Sidebar from '../../components/Sidebar';
import {Route, Routes} from 'react-router-dom';
import TopNavigation from '../../components/TopNavigation';
import Footer from '../../components/Footer';
import {PAGEROUTES} from '../../constants/routes';
import Transaction from './Transactions';
import NewTransaction from './Transactions/NewTransaction';

const MainApplication = (props) => {
  return (
    <>
      <Container>
        <TopNavigation />
        <Row>
          <Col className="g-0 mx-auto" md={2} lg={2}>
            <Sidebar />
          </Col>
          <Col md={10}>
            <Routes>
              <Route path={PAGEROUTES.Transaction} element={<Transaction />}></Route>
              <Route path={PAGEROUTES.NewTransaction} element={<NewTransaction />}></Route>
            </Routes>
          </Col>
          <Footer />
        </Row>
      </Container>
    </>
  );
};

export default MainApplication;
