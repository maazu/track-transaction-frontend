import React, {useState} from 'react';

import {Container} from 'react-bootstrap';

import NewClientForm from '../../../../components/Forms/Client/NewClientForm';

const NewClient = () => {
  return (
    <Container>
      <hr />
      <h3>New Transaction Form</h3>
      <hr />
      <NewClientForm />
      <hr />
    </Container>
  );
};

export default NewClient;
