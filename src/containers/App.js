import React from 'react';
import {Routes, Route, Navigate} from 'react-router-dom';
import {useSelector} from 'react-redux';
import '../styles/main.scss';

import Signin from './SignIn';
import MainApplication from './Application';

function App() {
  const auth = useSelector((state) => state.auth);

  return (
    <Routes>
      <Route path="/" element={<Navigate to={'/login'} />}></Route>
      {!auth.isLoggedIn && <Route path="/login" element={<Signin />}></Route>}
      {auth.isLoggedIn && <Route path="/login" element={<Navigate to={'/dashboard'} />}></Route>}
      {auth.isLoggedIn && <Route path="/dashboard/*" element={<MainApplication />}></Route>}
      <Route path="*" element={<Navigate to={'/login'} />}></Route>
    </Routes>
  );
}

export default App;
