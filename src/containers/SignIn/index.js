import React from 'react';
import {Container, Row, Col} from 'react-bootstrap';
import SigninForm from '../../components/Forms/SigninForm';
import './signin.module.scss';

const SignIn = () => {
  return (
    <>
      <Container>
        <Row>
          <Col lg={3} className="position-absolute top-50 start-50 translate-middle">
            <SigninForm />
          </Col>
        </Row>
      </Container>
    </>
  );
};
export default SignIn;
