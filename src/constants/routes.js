export const PAGEROUTES = {
  Dashboard: '/',
  Transaction: '/transaction',
  AllTransactions: '/view-all-transaction',
  NewTransaction: '/new-transaction',
  ModifyTransaction: '/update-transaction',
  TransactionDetails: '/transaction/details/',
};
