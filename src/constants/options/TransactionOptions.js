export const TransactionGenerateDate = new Date().toISOString();

export const TransactionType = {
  C: 'Credit',
  D: 'Debit',
};

export const TransactionPaymentMethods = {
  CH: 'Cash',
  BT: 'Bank Transfer',
  OP: 'Online Payment',
  TP: 'Terminal Payment (POS)',
  SP: 'SWAP Contract',
};

export const TransactionRegion = {
  CH: 'Within city',
  DM: 'Domestic',
  INTL: 'International',
};

export const TransactionStatus = {
  DUE: 'Due',
  CLEAR: 'Clear',
  PRP: 'Pending',
  CANCEL: 'Cancelled',
  PR: 'Reversed Payment',
  OTHER: 'Other',
};
