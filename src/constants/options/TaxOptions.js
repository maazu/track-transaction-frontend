export const TransactionGenerateDate = new Date().toISOString();

export const SalesTaxCodes = {
  GST: 'General Sales Tax (GST)',
  VAT: 'Value Added Tax (VAT)',
  FED: 'Federal Excise Duty (FED',
  CVT: 'Capital value tax(CVT)',
  PPT: 'Professional Tax',
  IT: 'Income Tax',
  CD: 'Custom Duty',
  SD: 'Stamp Duty',
  PT: 'Property Tax',
  CT: 'Council Tax',
};
