export const uiMessage = {
  serverUpgrade: 'We are having upgrades, please check back in few minutes.',
  serverDown: 'Server unavailable, could not connect to main server.',
  unauthorised: 'You are not authorised to access this resources.',
  invalidUser: 'Invalid user credentials, please try again',
};
