import axios from 'axios';
import {AuthorisedHeaderOptions} from '../../utils/request';

export const CreateTransaction = function (newTransactionData) {
  console.log(newTransactionData);
  return axios.post(`/api/transaction`, {newTransactionData}, AuthorisedHeaderOptions());
};

export const GetTransactions = function () {
  return axios.get(`/api/transaction`, AuthorisedHeaderOptions());
};
