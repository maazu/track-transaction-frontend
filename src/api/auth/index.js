import axios from 'axios';
import {standardHeaderOptions} from '../../utils/request';

const HandleLoginRequest = function (credentials) {
  return axios.post(`/api/auth/login`, {credentials}, standardHeaderOptions());
};

export default HandleLoginRequest;
