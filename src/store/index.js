import {configureStore} from '@reduxjs/toolkit';
import AuthReducer from './auth';
import notificationReducer from './uiNotification';

const store = configureStore({
  reducer: {auth: AuthReducer, notification: notificationReducer},
});

export default store;
