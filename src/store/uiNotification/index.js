import {createSlice} from '@reduxjs/toolkit';

const initialNotification = {
  type: null,
  message: '',
};

const uiNotificationSlice = createSlice({
  name: 'notification',
  initialState: initialNotification,
  reducers: {
    pushUiNotification(state, actions) {
      const payload = actions.payload;
      state.type = payload.type;
      state.message = payload.message;
    },
  },
});

export const uiNotificationActions = uiNotificationSlice.actions;
export default uiNotificationSlice.reducer;
