import {createSlice} from '@reduxjs/toolkit';
import {Navigate} from 'react-router-dom';
const localToken = localStorage.getItem('token');
let loggedInStatus = false;
if (localToken) {
  loggedInStatus = true;
}
const initialLoginState = {username: '', token: localToken, isLoggedIn: loggedInStatus};

const authSlice = createSlice({
  name: 'authentication',
  initialState: initialLoginState,
  reducers: {
    login(state, actions) {
      const payload = actions.payload;
      state.username = payload.username;
      state.token = payload.token;
      state.isLoggedIn = payload.isLoggedIn;
      localStorage.setItem('token', state.token);
    },
    logout(state) {
      state.token = null;
      localStorage.removeItem('token');
    },
  },
});

export const authActions = authSlice.actions;
export default authSlice.reducer;
