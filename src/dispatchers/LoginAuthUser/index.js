import {authActions} from '../../store/auth';

const LoginAuthUser = (dispatch, username, token) => {
  dispatch(authActions.login({username: username, token: token, isLoggedIn: true}));
};

export default LoginAuthUser;
