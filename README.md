> This repository contains the source code and documentation for TrackTransactions Project.
> This platform is inspired by salesforce, however as someone who has done some salesforce training, I think it is very difficult to get started for a small bussiness owner, so I decided to built a similar platform which work in a way that even a high school student can work on it. This project is currently under development.

## Table of Contents

- [Getting Started](#getting-started)
- [Prior Knowledge](#prior-knowledge)

## Prior Knowledge

Any knwoledge of following will enable to get the better understanding of project.

- Javascript (ES6 or above)
- SCSS
- Reactjs
- Redux Toolkit
- React Router (version 6)

## Getting Started

Clone the repo, run the following command to install the project dependencies:

```bash
npm install
```

Then to run the code, run the following command:

```bash
npm start
```

## Contributions

Each pull request must be connected to an issue, before creating a PR, please create an issue and attach it by creating a new the branch from the relvant feature branch.
